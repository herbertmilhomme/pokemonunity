![alt text](http://i.imgur.com/K9pgeKE.png)
# Pokemon Unity
Pokémon Unity Source Code by IIcolour Spectrum

  This is the source of Pokémon Unity.

  PKU was in development for about a year before I could
not continue working on the project. Rather than let it
die, I released the source as is.

  Naturally this leaves it in a fairly incomplete state,
albeit functional.

  There's a lot of legacy code left over from when I was
learning to use Unity, so some scripts (namely the PC 
and Bag interfaces) are functional though implemented
rather poorly.

  I'm hoping that the Pokémon Game Development Community
will be able to continue my work and eventually get this
project into a fully functional state.

---------------------------------------------------------

  If you need to contact me regarding the project for 
any reason, you can reach me via my YouTube channel, or
the subreddit /r/PokemonUnity although I can't guarantee
when I'll see it or how much I can help.
  I will try though.

---------------------------------------------------------

  Thank you to everyone who supported me during the 
development of Pokémon Unity. 
  I hope this source is useful.

## Cloning
**Please use Git LFS to clone** our .pdb files are huge and cannot be easily kept in a plain git repo. Learn why [here](https://blog.bitbucket.org/2016/07/18/git-large-file-storage-now-in-bitbucket-cloud/)

*This requires git and git lfs **install them in order*** 

**Set up your SSH too if you want to clone locally (How to [here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html))**

Simply do `git lfs clone git@bitbucket.org:lucasone/pokemonunity.git`

## Creator & Credits
Creator is [IIcolour Spectrum](https://www.reddit.com/user/IIcolour_Spectrum)

Git LFS is managed by LucasOne