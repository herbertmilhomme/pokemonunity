
		   ~~~ Pok�mon Unity ~~~
		        by IIColour
                                                  v1.0.0
---------------------------------------------------------

  This is the source of Pok�mon Unity.

  PKU was in development for about a year before I could
not continue working on the project. Rather than let it
die, I released the source as is.

  Naturally this leaves it in a fairly incomplete state,
albeit functional.

  There's a lot of legacy code left over from when I was
learning to use Unity, so some scripts (namely the PC 
and Bag interfaces) are functional though implemented
rather poorly.

  I'm hoping that the Pok�mon Game Development Community
will be able to continue my work and eventually get this
project into a fully functional state.

---------------------------------------------------------

  If you need to contact me regarding the project for 
any reason, you can reach me via my YouTube channel, or
the subreddit /r/PokemonUnity although I can't guarantee
when I'll see it or how much I can help.
  I will try though.

---------------------------------------------------------

  Thank you to everyone who supported me during the 
development of Pok�mon Unity. 
  I hope this source is useful.